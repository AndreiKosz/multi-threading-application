import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by Admin on 09.04.2017.
 */
public class ClientGenerator extends Thread {
    private int currentTime;
    private static int id;
    private Random random = new Random();
    private Client client;
    private int minProcessing;
    private int maxProcessing;
    private int minArrival;
    private int maxArrival;
    private BlockingQueue<Client> generator=new ArrayBlockingQueue<Client>(1);

    public ClientGenerator(int currentTime,int minProcessing,int maxProcessing,int minArrival,int maxArrival) {
        this.currentTime = currentTime;
        this.minProcessing=minProcessing;
        this.maxProcessing=maxProcessing;
        this.minArrival=minArrival;
        this.maxArrival=maxArrival;
        client = new Client(id++, random.nextInt(this.maxProcessing) + this.minProcessing, currentTime + random.nextInt(this.maxArrival) + this.minArrival);



    }




    public Client getClient() {

        return client;
    }


    }

