/**
 * Created by Admin on 14.03.2017.
 */
/**
 * Created by Admin on 13.03.2017.
 */
import java.awt.*;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;

public class Controller  {

    private int simulation2;
    private int minProcessing2,maxProcessing2;
    private int queuesNo;
    private int minArrival2,maxArrival2;
    private String str="ana";
    public Controller() {
    }

    @FXML
    // The reference of inputText will be injected by the FXML loader
    private TextField simulation,minProcessing,maxProcessing,queues,minArrival,maxArrival,avgTime,peekTime;

    // The reference of outputText will be injected by the FXML loader
    @FXML
    private TextArea log;
    @FXML
    private Label text1;

    // location and resources will be automatically injected by the FXML loader
    @FXML
    private URL location;

    @FXML
    private ResourceBundle resources;


    public void getSim(){
        simulation2=Integer.parseInt(simulation.getText());

    }

    public void getProcessing(){
        minProcessing2=Integer.parseInt(minProcessing.getText());
        maxProcessing2=Integer.parseInt(maxProcessing.getText());

    }

    public void getQueues(){
        queuesNo=Integer.parseInt(queues.getText());
    }

    public void getArrvial(){
        minArrival2=Integer.parseInt(minArrival.getText());
        maxArrival2=Integer.parseInt(maxArrival.getText());
    }
    public void start(){
        SimulationManager sim = new SimulationManager(queuesNo, simulation2, minProcessing2,maxProcessing2,minArrival2,maxArrival2,log,avgTime,peekTime);
       // System.out.println(queuesNo+" "+simulation2);
        sim.start();


    }
    public void setLog(String str){
        log.setText(str);
    }




}




