/**
 * Created by Admin on 01.04.2017.
 */
public class Client {
    private int processingTime;
    private int arrivalTime;
    private int finishTime;
    private int id;

    public Client(int id,int processingTime,int arrivalTime){
        this.processingTime=processingTime;
        this.arrivalTime=arrivalTime;
        this.finishTime=processingTime+arrivalTime;
        this.id=id;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public int getFinishTime() {
        return finishTime;
    }

    public int getProcessingTime() {
        return processingTime;
    }

    public void setFinishTime(int finishTime) {
        this.finishTime = finishTime;
    }

    public int getIdClient() {
        return id;
    }
}
