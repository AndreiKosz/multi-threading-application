import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.util.Random;

/**
 * Created by Admin on 09.04.2017.
 */
    import java.util.Random;

import static java.lang.Thread.sleep;
import static java.sql.JDBCType.NULL;

/**
     * Created by Admin on 03.04.2017.
     */
    public class SimulationManager extends Thread {

        private int nrCase;
        private int simulationTime;
        private Coada queue[];
        private int minPorcessing;
        private int maxProcessing;
        private int minArrival;
        private int maxArrival;
        private TextArea log;
        private TextField avgTime;
        private TextField peekTime;



        public SimulationManager(int nrCase, int simulationTime, int minProcessing, int maxProcessing, int minArrival, int maxArrival, TextArea log, TextField avgTime,TextField peekTime) {
            this.nrCase = nrCase;
            this.simulationTime = simulationTime;
            this.maxProcessing=maxProcessing;
            this.minPorcessing=minProcessing;
            this.minArrival=minArrival;
            this.maxArrival=maxArrival;
            this.log=log;
            this.avgTime=avgTime;
            this.peekTime=peekTime;
            this.queue=new Coada[nrCase];
            for (int i = 0; i < nrCase; i++) {
                this.queue[i] = new Coada(i,log);
                this.queue[i].start();
            }
        }

        public int timeSchedule() {
            int min;
            int position = 0;
            min = queue[0].getWaitingTime();
            for (int i = 1; i < nrCase; i++) {
                int time = queue[i].getWaitingTime();
                if (time < min) {
                    min = time;
                    position = i;
                }
            }
            return position;

        }

        public void run() {
            String str;
            String str2;
            try {
                int currentTime = 0;
                int index=0;
                while (currentTime < simulationTime) {
                    ClientGenerator generator=new ClientGenerator(currentTime,minPorcessing,maxProcessing,minArrival,maxArrival);

                    Client c = generator.getClient();
                    index = timeSchedule();
                    //System.out.println(currentTime);
                    queue[index].insertClient(c,index,minArrival,maxArrival);
                    sleep( 1000);
                    currentTime++;
                }
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
            str=Integer.toString(Coada.peekTime);
            str2=Double.toString(Coada.maxTime/Coada.clientNumber);
            peekTime.setText(str);
            avgTime.setText(str2);
            //log.appendText("Timpul mediu "+(Coada.maxTime/Coada.clientNumber));

        }
    }

