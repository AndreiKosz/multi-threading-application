import javafx.application.Platform;
import javafx.scene.control.TextArea;

import javax.xml.soap.Text;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import static java.lang.Thread.sleep;

/**
 * Created by Admin on 08.04.2017.
 */
public class Coada extends Thread
{
    private BlockingQueue<Client>coada=new ArrayBlockingQueue<Client>(10);
    private int id;
    private volatile  Integer waitingTime=0;
    private volatile  boolean isRunning=true;
    private TextArea log;
    private String str="";
    public static double clientNumber;
    public static double maxTime;
    public static int maxPersons=0;
    public static int currentPersons;
    public static int peekTime;

    public Coada(int id,TextArea log){
        this.id=id;
        this.log=log;
    }

    public void insertClient(Client c,int index,int minArrival,int maxArrival) throws InterruptedException{
        Random random=new Random();
            coada.put(c);
            currentPersons++;
            if(currentPersons>=maxPersons){
                maxPersons=currentPersons;
                peekTime=c.getArrivalTime();
            }
           // System.out.println("Clientul " + c.getIdClient() + " Aduagat la casa " + index);
        Platform.runLater(()-> {

                    this.log.appendText("Clientul " + c.getIdClient() + " adaugat la casa " + index + " : timpul de sosire " + c.getArrivalTime() + " ,timpul de procesare" + c.getProcessingTime() + '\n');
                });
            clientNumber++;
            c.setFinishTime(c.getFinishTime() + waitingTime);
            waitingTime += c.getProcessingTime();
            maxTime += c.getProcessingTime();
            sleep(1000*random.nextInt(maxArrival)+minArrival);

    }


    public Integer getWaitingTime() {
        return waitingTime;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }

    public void processing(Client c){
        Platform.runLater(()-> {

            this.log.appendText("Clientul " + c.getIdClient() + " a fost procesat de casa " + this.id + " avand timpul de finalizare de: " + c.getFinishTime() + '\n');
        });

    }

    public void run() {

        try{
            Client c;
            while(isRunning){
                c=coada.take();

                sleep(1000*c.getProcessingTime());
                currentPersons--;
                waitingTime-=c.getProcessingTime();
              // System.out.println("Clientul "+c.getIdClient()+" a fost procesat de casa "+this.id+" avand timpul de finalizare de: "+c.getFinishTime());
                processing(c);



            }
        }
        catch(InterruptedException e){
            System.out.println("Coada este intrerupta");

        }


    }
}
